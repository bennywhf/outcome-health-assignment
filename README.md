# Growth Solutions Software Engineer Code Test


The test covers the following:
* General Python/Django/DRF Web Development Proficiency
* General JavaScript Web Development Proficiency
* API design
* Comfort with working in a docker environment
* Ability to write tests!!! and document code

Note:
Feel free to use any 3rd party libraries for any portion of this. In some cases, it is encouraged. Part of the follow up will be discussion why you chose to use the libraries that you chose. Some of this test is intentionally open ended, so treat it as if it would go into production (after a couple hours of work) and be a long lasting application (Remember to test)

Best of luck and have fun!


## Assignment

There are four database tables represented in models in `core/models.py`. Currently there is no API on top of those models. Building out some of that API is your task as well as a client app using angular on top of those endpoints. Styling is not a requirement but if you have time go for it.

This assignment should not take more than 2-3 hours. The goal of the assignment is not to tease out the test taker's deep knowledge of the DRF API, rather to see how well one can work in a web environment with consideration of how that work will interact with other parts of the stack. If you are running out of time feel free to write some form of psuedocode to show how you would have done it. 

### Cards:

#### Card 1: Feature

A list view of the campaigns that has the campaign information as well as the product name.

API:
Create an API endpoint that allows a user to GET multiple campaigns. The URL via query params should also be able to filter on:
1. Product Name
2. Name
3. Start Date (greater than, less than or equal to)
4. End Date (greater than, less than or equal to)

Note: Feel free to use a third party library or a Django Rest Framework Extension for this. The end goal is user functionality.

Client
If you have time add the filtering and styling to the client although that is outside the scope of the assignment

#### Card 2: Feature

Create an API endpoint that allows a user to POST a new campaign to a preexisting product through a form in a modal in the frontend that is accessible on the campaign list view. 

The one piece of validation required is that a campaign for a certain product cannot a have date overlap with another
 campaign for the same product. If a user attempts this, they should see some type of validation error in the modal
 
## Set up

### Docker
Make sure you have docker installed. Here is the link for macOS: https://download.docker.com/mac/stable/Docker.dmg

For other platforms reference https://docs.docker.com/install/

### NVM

Install NVM https://github.com/nvm-sh/nvm to manage node versions

#### Commands

In the project directory run the follow to build/setup your local environment

```bash
docker-compose build
```

To start your containers
```bash
docker-compose up -d
```

To run django migrations 
```bash
docker-compose exec web python manage.py migrate
```

To create super user in order to access admin

```bash
docker-compose exec web python manage.py createsuperuser
```

To install npm, nvm and angular
```bash
cd angular-app
nvm install 10.16
npm install
```

Angular live reload
```bash
cd angular-app
ng serve
```

To see the logs
```bash
docker-compose logs -f  
```


## Note on React
If you are more comfortable with react feel free to use it for this assignment.
