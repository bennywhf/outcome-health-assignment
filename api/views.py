from rest_framework.views import APIView
from rest_framework.response import Response
from core.models import Campaign
from core.controller import CoreController
from core.serializers import CampaignSerializer
import dateutil.parser


class CampaignsView(APIView):
    """
    Campaigns View.
    """

    def get(self, request):
        """
        Return a list of Campaigns.
        """
        if request.GET:
            try:
                return Response(
                    CampaignSerializer(
                        CoreController().get_campaign_list(
                            {
                                key: self.parse_param(key, value)
                                for key, value in request.GET.items()
                            }
                        ),
                        many=True,
                    ).data
                )
            except Exception as e:
                pass

        return Response(CampaignSerializer(Campaign.objects.all(), many=True).data)

    @staticmethod
    def parse_param(key, param):
        """
        :param key:
        :param param:
        :return: string | datetime

        If the input key has "date" in it, then we try to get a datetime object from it. if that fails, then we just take the text value.
        """
        if "date" in key:
            return dateutil.parser.parse(param).date() or param
        return param

    def post(self, request):
        name = request.POST["name"]
        product = request.POST["product"]
        start = self.parse_param("date", request.POST["start_date"])
        end = self.parse_param("date", request.POST["end_date"])

        try:
            return Response(
                CampaignSerializer(
                    CoreController().add_campaign(name, product, start, end)
                ).data,
                status=201,
            )
        except Exception as e:
            return Response(str(e), status=400)
