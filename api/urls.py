from django.urls import path
from .views import CampaignsView

urlpatterns = [path("campaign", CampaignsView.as_view())]
