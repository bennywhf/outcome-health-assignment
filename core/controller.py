from .models import Campaign, Product


class CoreController:
    def add_campaign(self, name, product_id, start_date, end_date):
        campaign_exists = (
            Campaign.objects.filter(
                product__id=product_id,
                start_date__lte=start_date,
                end_date__gt=start_date,
            ).count()
            + Campaign.objects.filter(
                product__id=product_id, start_date__lt=end_date, end_date__gte=end_date
            ).count()
            > 0
        )

        if campaign_exists:
            raise Exception("CAMPAIGN_EXISTS")

        if start_date > end_date:
            raise Exception("BAD_DATE_RANGE")

        product = Product.objects.get(id=product_id)
        campaign = Campaign.objects.create(
            name=name, product=product, start_date=start_date, end_date=end_date
        )

        return campaign

    def get_campaign_list(self, params):
        return Campaign.objects.filter(**params)
