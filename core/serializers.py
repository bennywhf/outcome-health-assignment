from rest_framework import serializers
from .models import Campaign, Product, Account


class AccountSerializer(serializers.ModelSerializer):
    salesperson = serializers.SerializerMethodField()

    class Meta:
        model = Account
        fields = ["salesperson", "name", "id"]

    def get_salesperson(self, obj):
        return obj.salesperson.username


class ProductSerializer(serializers.ModelSerializer):
    account = AccountSerializer()

    class Meta:
        model = Product
        fields = ["account", "name", "id"]


class CampaignSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = Campaign
        fields = ["name", "start_date", "end_date", "product", "id"]
