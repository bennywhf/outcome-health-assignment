from .serializers import CampaignSerializer
from .models import Campaign


def get_serialized_campaigns():
    return CampaignSerializer(Campaign.objects.all(), many=True).data
